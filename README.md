Парсер для access логов nginx. Csv экспортируется из kibana. Группирует и оттдает csv + pie chart

`go build -o ./bin/app`

`bin/app -f <filepath>.csv -o ~/<filename>`

Flags

-f - filepath (by default data.csv) 

-o - output filePath (by default "output")
