package csv

import (
	"access-log-chart/src/parser"
	"encoding/csv"
	"fmt"
	"io"
	"os"
	"strconv"
)

func Parse(file *os.File) [][]string {
	reader := csv.NewReader(file)

	var data [][]string

	for {
		row, err := reader.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			panic(err.Error())
		}
		data = append(data, row)
	}
	return data
}

func Export(logs []parser.Log, output string) {
	f, e := os.Create(output + ".csv")
	defer f.Close()

	if e != nil {
		panic(e.Error())
	}

	writer := csv.NewWriter(f)
	defer writer.Flush()

	writer.Write([]string{"URL", "PERCENT", "COUNT"})

	for _, log := range logs {
		perc := fmt.Sprintf("%.1f", log.Percent)
		count := strconv.Itoa(log.Count)
		row := []string{log.Url, perc, count}
		writer.Write(row)
	}

	total := logs[0].Count / int(logs[0].Percent) * 100
	writer.Write([]string{"ALL", "100", strconv.Itoa(total)})
}
