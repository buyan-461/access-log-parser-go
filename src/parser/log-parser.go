package parser

import (
	"fmt"
	"regexp"
	"sort"
	"strings"
)

type LogParser struct {
	Data [][]string
}

type Log struct {
	Url     string
	Percent float64
	Count   int
}

const URL_REGEXP = `(GET |POST |PUT |DELETE )(\/v1\/[\w+\-]+\/[\w+|\-]+)([\/|\?]?)`
const DYNAMIC_URL_REGEXP = `\/v1\/(record|list-filter)\/(.+)`

func Parse(data [][]string) []Log {
	result := make(map[string]int)
	countAll := 0

	reg, _ := regexp.Compile(URL_REGEXP)
	dynReg, _ := regexp.Compile(DYNAMIC_URL_REGEXP)
	logIndex := findLogColumnIndex(data)

	for _, item := range data {
		url, ok := getUrl(item[logIndex], *reg, *dynReg)
		if ok == false {
			continue
		}
		_, isset := result[url]
		if isset {
			result[url]++
		} else {
			result[url] = 1
		}
		countAll++
	}

	logs := []Log{}

	for url, count := range result {
		perc := float64(count) / float64(countAll) * 100
		logs = append(logs, Log{url, perc, count})
	}

	fmt.Println("Count all:", countAll)
	sortLogs(logs)
	return logs
}

func getUrl(cell string, reg regexp.Regexp, dynReg regexp.Regexp) (string, bool) {
	match := reg.FindStringSubmatch(cell)

	if len(match) == 0 {
		return "", false
	}
	url := match[1] + match[2]
	dynMatch := dynReg.FindStringSubmatch(url)

	if len(dynMatch) != 0 {
		url = strings.Replace(url, dynMatch[2], "<tableName>", 1)
	}

	return url, true
}

func sortLogs(logs []Log) {
	sort.SliceStable(logs, func(i, j int) bool {
		return logs[i].Percent > logs[j].Percent
	})
}

func findLogColumnIndex(data [][]string) int {
	for i := 0; i < 10; i++ {
		for ind, cell := range data[i] {
			ok, _ := regexp.MatchString(URL_REGEXP, cell)
			if ok {
				return ind
			}
		}
	}
	panic("Log column not found.")
}
