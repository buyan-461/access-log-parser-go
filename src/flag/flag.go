package flag

import "flag"

type Flag struct {
	Filepath     string
	Output       string
	GroupPercent float64
}

func Init() Flag {
	filepath := flag.String("f", "data.csv", "filepath")
	output := flag.String("o", "output", "output filepath")
	perc := flag.Float64("perc", 2, "chart group percent")
	flag.Parse()

	return Flag{
		Filepath:     *filepath,
		Output:       *output,
		GroupPercent: *perc,
	}
}
