package chart

import (
	"access-log-chart/src/parser"
	"os"

	"github.com/wcharczuk/go-chart"
)

func RenderPieChart(logs []parser.Log, groupPercent float64, output string) {
	chartData := groupOther(logs, groupPercent)

	pie := chart.PieChart{
		Width:  1024,
		Height: 1024,
		Values: chartData,
	}

	img, _ := os.Create(output + ".png")
	defer img.Close()

	pie.Render(chart.PNG, img)
}

func groupOther(logs []parser.Log, groupPercent float64) []chart.Value {
	grouped := []chart.Value{}
	other := chartItem("Other", 0)

	for _, log := range logs {
		if log.Percent < groupPercent {
			other.Value += log.Percent
		} else {
			grouped = append(grouped, chartItem(log.Url, log.Percent))
		}
	}

	return append(grouped, other)
}

func chartItem(label string, value float64) chart.Value {
	return chart.Value{
		Label: label,
		Value: value,
		Style: chart.Style{FontSize: 8},
	}
}
