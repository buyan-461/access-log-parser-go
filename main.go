package main

import (
	"access-log-chart/src/chart"
	"access-log-chart/src/csv"
	"access-log-chart/src/flag"
	"access-log-chart/src/parser"
	"fmt"
	"os"
	"time"
)

func main() {
	st := time.Now()
	config := flag.Init()
	file, err := os.Open(config.Filepath)

	if err != nil {
		panic(err.Error())
	}

	data := csv.Parse(file)
	logs := parser.Parse(data)

	chart.RenderPieChart(logs, config.GroupPercent, config.Output)
	csv.Export(logs, config.Output)

	fmt.Println("Timing: ", time.Since(st))
}
